/**
 * This package is my solution to the Paging Mission Control
 * coding assessment given to me as part of an interview by Enlighten.
 * The challenge can be found at:
 *  https://gitlab.com/enlighten-challenge/paging-mission-control.
 *
 * @author Joshua Snider - josh@joshuasnider.com
 * @version 1.0
 */
package com.joshuasnider.missioncontrol;
