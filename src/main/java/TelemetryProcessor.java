/**
 * A solution to https://gitlab.com/enlighten-challenge/paging-mission-control
 *
 * The basic idea is that we create a TelemetryProcessor that reads all of the
 * telemetry data into a dictionary of lists, then we go through each list
 * finding all of the times the limits were violated and make alerts.
 */
package com.joshuasnider.missioncontrol;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A class that stores all of the telemetry data for a satellite regardless of
 * what component it is from.
 */
class TelemetryData {

    /**
     * The series of battery data we get from the satellite.
     */
    private BatterySeries battery;

    /**
     * The series of thermostat data we get from the satellite.
     */
    private ThermostatSeries thermostat;

    TelemetryData() {
        this.battery = new BatterySeries();
        this.thermostat = new ThermostatSeries();
    }

    public BatterySeries getBattery() {
        return battery;
    }

    public ThermostatSeries getThermostat() {
        return thermostat;
    }
}

/**
 * A wrapper class for ArrayLists of telemetry data. It has
 * a few variables and methods for checking for limit violations.
 */
abstract class TelemetrySeries extends ArrayList<TelemetryEntry> {

    abstract List<AlertMessage> checkViolation(String satelliteId);

    /**
     * How many times can the limit be exceeded without sounding the alarm?
     */
    private final int maxViolators = 3;

    /**
     * How far in the past (in minutes) do we need to check for violations?
     */
    private final int windowSize = 5;

    protected List<AlertMessage> checkViolation(final String satelliteId,
            final String label, final String component, boolean isLimitHigh) {
        List<AlertMessage> alertMessages = new ArrayList<>();
        List<TelemetryEntry> violators = new ArrayList<>();
        for (TelemetryEntry cur : this) {
            if ((isLimitHigh && cur.getRawValue() < cur.getLimit())
                || (!isLimitHigh && cur.getRawValue() > cur.getLimit())) {
                violators.add(cur);
            }
            while (violators.size() > 0
                && cur.getTimestamp().isAfter(violators.get(0).getTimestamp()
                    .plusMinutes(windowSize))) {
                violators.remove(0);
            }
            if (violators.size() >= maxViolators) {
                AlertMessage alertMessage = new AlertMessage(
                    Integer.parseInt(satelliteId), label, component,
                    violators.get(0).getTimestamp().toString());
                alertMessages.add(alertMessage);
            }
        }

        return alertMessages;
    }
}

/**
 * Contains battery data and the specific setup necessary to
 * identify battery violations.
 */
class BatterySeries extends TelemetrySeries {

    /**
     * An abbreviation for battery.
     */
    public static final String ABBR = "BATT";

    public List<AlertMessage> checkViolation(String satelliteId) {
        return checkViolation(satelliteId, "RED LOW", ABBR, true);
    }
}

/**
 * Contains thermostat data and the specific setup necessary to
 * identify thermostat violations.
 */
class ThermostatSeries extends TelemetrySeries {

    /**
     * An abbreviation for thermostat.
     */
    public static final String ABBR = "TSTAT";

    public List<AlertMessage> checkViolation(final String satelliteId) {
        return checkViolation(satelliteId, "RED HIGH", ABBR, false);
    }
}

/**
 * Contains a single sensor reading.
 */
class TelemetryEntry {

    /**
     * The time when this sensor reading took place.
     */
    private LocalDateTime timestamp;

    /**
     * The reading of the sensor at the given time.
     */
    private double rawValue;

    /**
     * The value of rawValue which will trigger an alert.
     */
    private double limit;

    TelemetryEntry(LocalDateTime timestamp, double rawValue, double limit) {
        this.timestamp = timestamp;
        this.rawValue = rawValue;
        this.limit = limit;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public double getRawValue() {
        return rawValue;
    }

    public double getLimit() {
        return limit;
    }
}

/**
 * A message announcing that a violation has been detected and some details
 * of it.
 */
class AlertMessage {

    /**
     * A number uniquely identifying the satellite associated with this alert.
     */
    private int satelliteId;

    /**
     * A label for the alert which says what kind it is.
     */
    private String severity;

    /**
     * The name of the component of the satellite that triggered the alert.
     */
    private String component;

    /**
     * The time as per TelemetryProcessor.FORMATTER of the limit violation.
     */
    private String timestamp;

    AlertMessage(final int satelliteId, final String severity,
                 final String component, final String timestamp) {
        this.satelliteId = satelliteId;
        this.severity = severity;
        this.component = component;
        this.timestamp = timestamp;
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public String getSeverity() {
        return severity;
    }

    public String getComponent() {
        return component;
    }

    public String getTimestamp() {
        return timestamp;
    }
}

/**
 * The main class for solving
 * https://gitlab.com/enlighten-challenge/paging-mission-control.
 */
public class TelemetryProcessor {

    /**
     * The date format used for satellite input.
     */
    private static final DateTimeFormatter FORMATTER =
        DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS");

    /**
     * The map we use to store out telemetry data. The keys are
     * the ids of the satellites.
     */
    private Map<String, TelemetryData> telemetryDataMap;

    /**
     * Create a TelemetryProcessor based off of a telemetry file.
     *
     * @param filePath - the telemetry file to use as a source of data.
     */
    public TelemetryProcessor(final String filePath) {
        telemetryDataMap = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(
                                     new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split("\\|");

                LocalDateTime timestamp =
                    LocalDateTime.parse(parts[0], FORMATTER);
                String satelliteId = parts[1];
                double redHighLimit = Double.parseDouble(parts[2]);
                double yellowHighLimit = Double.parseDouble(parts[3]);
                double yellowLowLimit = Double.parseDouble(parts[4]);
                double redLowLimit = Double.parseDouble(parts[5]);
                double rawValue = Double.parseDouble(parts[6]);
                String component = parts[7];

                TelemetryData telemetryData = telemetryDataMap.computeIfAbsent(
                    satelliteId, k -> new TelemetryData());

                BatterySeries battery = telemetryData.getBattery();
                ThermostatSeries thermostat = telemetryData.getThermostat();
                if (BatterySeries.ABBR.equals(component)) {
                    battery.add(new TelemetryEntry(
                        timestamp, rawValue, redLowLimit));
                } else if (ThermostatSeries.ABBR.equals(component)) {
                    thermostat.add(new TelemetryEntry(
                        timestamp, rawValue, redHighLimit));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Check the TelemetryData for violations and report them as AlertMessages.
     * @return a list of alert messages summarizing the violations found.
     */
    public List<AlertMessage> getAlerts() {

        List<AlertMessage> alerts = new ArrayList<>();

        for (Map.Entry<String, TelemetryData> entry
                : telemetryDataMap.entrySet()) {
            String satelliteId = entry.getKey();
            TelemetryData telemetryData = entry.getValue();
            BatterySeries battery = telemetryData.getBattery();
            ThermostatSeries thermostat = telemetryData.getThermostat();

            alerts.addAll(battery.checkViolation(satelliteId));
            alerts.addAll(thermostat.checkViolation(satelliteId));
        }

        return alerts;
    }

    /**
     * Take an input file from the command line and issue alerts if it
     * violates the predefined conditions.
     * @param args - Java's command line args. It should either be a filename
     *               with the desired input or empty to go with the default.
     *
     */
    public static void main(String[] args) {
        String filePath = "src/test/resources/telemetry.txt";
        if (args.length > 0) {
            filePath = args[0];
        }
        List<AlertMessage> alertMessages =
            new TelemetryProcessor(filePath).getAlerts();

        // Convert the alert messages to JSON and print them
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonOutput = gson.toJson(alertMessages);
        System.out.println(jsonOutput);
    }

}
