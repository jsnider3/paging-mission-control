package com.joshuasnider.missioncontrol;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import java.util.List;

/**
 * The test case for TelemetryProcessor.java.
 * All test cases consist of loading an input file
 * and asserting the correct number of alerts is generated.
 * With a few checks to ensure the alerts have the proper
 * form.
 */
class TestTelemetry {

    /**
     * If there are three sensor readings that violate the limit within a five
     * minute period, then generate an alert.
     */
    @Test
    void testEasyFail() {
        List<AlertMessage> alertMessages = new TelemetryProcessor("src/test/resources/easy_fail.txt").getAlerts();
        assertEquals(1, alertMessages.size());
        AlertMessage alert = alertMessages.get(0);
        assertEquals(1001, alert.getSatelliteId());
        assertEquals("TSTAT", alert.getComponent());
        assertEquals("RED HIGH", alert.getSeverity());
        assertEquals("2018-01-01T23:01:05.001", alert.getTimestamp());
    }

    /**
     * If there are three sensor readings that don't violate the limit,
     * then don't generate an alert.
     */
    @Test
    void testEasySuccess() {
        List<AlertMessage> alertMessages = new TelemetryProcessor("src/test/resources/easy_success.txt").getAlerts();
        assertEquals(0, alertMessages.size());
    }

    /**
     * From the requirements:
     * Ingest status telemetry data and create alert messages for the following violation conditions:
     *     If for the same satellite there are three battery voltage readings that are under the red low limit within a five minute interval.
     *     If for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.
     * 
     * This strongly implies to me that we should not generate an alert if the
     * sensor reading is equal to the limit and that's what this test case
     * checks.
     */
    @Test
    void testEqualExceeds() {
        List<AlertMessage> alertMessages = new TelemetryProcessor("src/test/resources/equal_exceeds.txt").getAlerts();
        assertEquals(0, alertMessages.size());
    }

    /**
     * Test that our behavior looks like the provided test case.
     */
    @Test
    void testTelemetryText() {
        List<AlertMessage> alertMessages = new TelemetryProcessor("src/test/resources/telemetry.txt").getAlerts();
        /* Correct output is:
        [
            {
                "satelliteId": 1000,
                "severity": "RED HIGH",
                "component": "TSTAT",
                "timestamp": "2018-01-01T23:01:38.001Z"
            },
            {
                "satelliteId": 1000,
                "severity": "RED LOW",
                "component": "BATT",
                "timestamp": "2018-01-01T23:01:09.521Z"
            }
        ]*/
        assertEquals(2, alertMessages.size());
        for (AlertMessage alert : alertMessages) {
            assertEquals(1000, alert.getSatelliteId());
            if (alert.getComponent().equals("TSTAT")) {
                assertEquals("RED HIGH", alert.getSeverity());
                assertEquals("2018-01-01T23:01:38.001", alert.getTimestamp());
            }
            if (alert.getComponent().equals("BATT")) {
                assertEquals("RED LOW", alert.getSeverity());
                assertEquals("2018-01-01T23:01:09.521", alert.getTimestamp());
            }
       }
    }

    /**
     * We should not generate an alert if the three out-of-bounds telemetry
     * readings are not within five minutes of each other.
     */
    @Test
    void testViolationGap() {
        List<AlertMessage> alertMessages = new TelemetryProcessor("src/test/resources/violation_gap.txt").getAlerts();
        assertEquals(0, alertMessages.size());
    }
 
    /**
     * We should generate multiple alerts if there are multiple sets of three
     * that violate the limits. This is ambiguous in the instructions, but
     * Danielle Watson said they wanted to see me solve it the way I think
     * is best.
     */
    @Test
    void testMultAlerts() {
        List<AlertMessage> alertMessages = new TelemetryProcessor("src/test/resources/mult_alerts.txt").getAlerts();
        assertEquals(3, alertMessages.size());
    }
}